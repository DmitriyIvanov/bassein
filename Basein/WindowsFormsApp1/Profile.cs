﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class Profile : Form
    {
        public Profile(string fio, string role)
        {
            InitializeComponent();
            label1.Text = fio;
            label2.Text = role;
            using (sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter(Raspisnie, sqlConnection);

                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);

                dataGridView1.DataSource = dataSet.Tables[0];
            }
            if (label2.Text == "Тренер")
            {
                button5.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
            }

        }
        string FIO, ROLE;
        string connetion = "Data Source = localhost; Initial Catalog = Бассейн_Иванов1; Integrated Security = true";
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        SqlConnection sqlConnection;
        private void button8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        string Raspisnie = "select День_недели, Время from Расписание_занятий";
        private void button1_Click(object sender, EventArgs e)
        {
            using (sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter(Raspisnie, sqlConnection);

                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);

                dataGridView1.DataSource = dataSet.Tables[0];
            }
        }
        string Group = "select Наименование from Группы";
        private void button2_Click(object sender, EventArgs e)
        {
            using (sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter(Group, sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                dataGridView1.DataSource = dataSet.Tables[0];
            }
        }

        string pool = "select Наименование from Виды_бассейнов";
        private void button3_Click(object sender, EventArgs e)
        {
            using (sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter(pool, sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                dataGridView1.DataSource = dataSet.Tables[0];
            }
        }
        string abonement = "select Количество_занятий, Стоимость from Абонементы";
        private void button4_Click(object sender, EventArgs e)
        {
            using (sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter(abonement, sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                dataGridView1.DataSource = dataSet.Tables[0];
            }
        }
        addGroup addGroup1;
        private void button6_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            addGroup1 = new addGroup(FIO,ROLE);
            this.Hide();
            addGroup1.Show();
        }

        Авторизация авторизация;
        AddTeachers addTeachers;
        addTickets addTickets1;
        private void button7_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            addTickets1 = new addTickets(FIO, ROLE);
            this.Hide();
            addTickets1.Show();
        }

        Students students;
        private void button9_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            students = new Students(FIO,ROLE);
            this.Hide();
            students.Show();
        }

        addStudents addStudents;
        private void button10_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            addStudents = new addStudents(FIO,ROLE);
            this.Hide();
            addStudents.Show();
        }

        private void Profile_FormClosed(object sender, FormClosedEventArgs e)
        {
            авторизация = new Авторизация();
            this.Hide();
            авторизация.Show();
        }
        deleteWorker deleteWorker1;
        private void button8_Click_1(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            deleteWorker1 = new deleteWorker(FIO,ROLE);
            this.Hide();
            deleteWorker1.Show();
        }
        alteruSportsman alteruSportsman;
        private void button11_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            alteruSportsman = new alteruSportsman(FIO, ROLE);
            this.Hide();
            alteruSportsman.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FIO = label1.Text;
            ROLE = label2.Text;
            addTeachers = new AddTeachers(FIO,ROLE);
            this.Hide();
            addTeachers.Show();
        }
    }
}
