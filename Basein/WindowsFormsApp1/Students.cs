﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class Students : Form
    {
        string FIO, ROLE;
        public Students(string fio, string role)
        {
            InitializeComponent();
 
            FIO = fio;
            ROLE = role;
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Наименование as Группа from Группы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataSet.Tables[0].Rows[i][0].ToString());
                }
                
            }
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                SqlDataAdapter sqlDataAdapter1 = new SqlDataAdapter($"select Фамилия, Имя, Отчество, Количество_пропусков, Абонементы.Количество_занятий, Группы.Наименование as Группа from Клиенты inner join Группы on Клиенты.Код_группы = Группы.Код_группы inner join Абонементы on Абонементы.Код_абонемента = Клиенты.Код_абонемента", sqlConnection);
                DataSet dataSet1 = new DataSet();
                sqlDataAdapter1.Fill(dataSet1);
                dataGridView1.DataSource = dataSet1.Tables[0];

            }
        }

        string connect = "Data Source = localhost;Initial Catalog = Бассейн_Иванов1;Integrated Security = true";
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        Profile profile;
        private void Students_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(FIO, ROLE);
            this.Hide();
            profile.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using(SqlConnection Connection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_группы, Наименование as Группа from Группы", Connection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(connect))
                        {
                            SqlDataAdapter sqlDataAdapter1 = new SqlDataAdapter($"select Фамилия, Имя, Отчество, Количество_пропусков, Абонементы.Количество_занятий, Группы.Наименование as Группа from Клиенты inner join Группы on Клиенты.Код_группы = Группы.Код_группы inner join Абонементы on Абонементы.Код_абонемента = Клиенты.Код_абонемента where Клиенты.Код_группы = {dataSet.Tables[0].Rows[i][0].ToString()}", sqlConnection);
                            DataSet dataSet1 = new DataSet();
                            sqlDataAdapter1.Fill(dataSet1);
                            dataGridView1.DataSource = dataSet1.Tables[0];

                        }
                    }
                }
               
            }
        }



    }
}
