﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class addStudents : Form
    {
        string FIO, ROLE;
        public addStudents(string fio, string role)
        {
            InitializeComponent();
            FIO = fio;
            ROLE = role;
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_группы, Наименование from Группы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataSet.Tables[0].Rows[i][1].ToString());
                }
                sqlDataAdapter = new SqlDataAdapter("select Код_абонемента, Стоимость from Абонементы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    comboBox2.Items.Add(dataSet.Tables[0].Rows[i][1].ToString());
                }
            }
        }
        string connect = "Data Source = localhost;Initial Catalog = Бассейн_Иванов1;Integrated Security = true";
        Profile profile;
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        SqlCommand sqlCommand;
        private void addStudents_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(FIO, ROLE);
            this.Hide();
            profile.Show();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word !=8)
            {
                e.Handled = true;
            }
        }
        string ID1, ID2;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_группы, Наименование from Группы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        ID1 = dataSet.Tables[0].Rows[i][0].ToString();
                    }
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select Код_абонемента, Стоимость from Абонементы", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (comboBox2.Text == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        ID2 = dataSet.Tables[0].Rows[i][0].ToString();
                    }
                }
            }
        }
        Students students;
        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                sqlCommand = new SqlCommand("insert into Клиенты(Фамилия, Имя, Отчество, Количество_пропусков, Код_группы, Код_абонемента) values (@pham, @imya, @otch, @kol, @kod1, @kod2)",sqlConnection);
                sqlCommand.Parameters.AddWithValue("@pham",textBox1.Text);
                sqlCommand.Parameters.AddWithValue("@imya",textBox2.Text);
                sqlCommand.Parameters.AddWithValue("@otch",textBox3.Text);
                sqlCommand.Parameters.AddWithValue("@kol",textBox4.Text);
                sqlCommand.Parameters.AddWithValue("@kod1",ID1);
                sqlCommand.Parameters.AddWithValue("@kod2", ID2);
                sqlConnection.Open();
                try
                {
                    sqlCommand.ExecuteNonQuery();
                    MessageBox.Show("Человек добавлен!");
                    students = new Students(FIO,ROLE);
                    this.Hide();
                    students.Show();
                }
                catch
                {
                    MessageBox.Show("Добавить не удалось!");
                }
            }
        }
    }
}
