﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class alteruSportsman : Form
    {
        string FIO, ROLE;
        public alteruSportsman(string fio, string role)
        {
            InitializeComponent();
            FIO = fio;
            ROLE = role;
        }
        Profile profile;
        private void alteruSportsman_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(FIO, ROLE);
            this.Hide();
            profile.Show();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char Word = e.KeyChar;
            if (!Char.IsDigit(Word) && Word != 8)
            {
                e.Handled = true;
            }
        }
    }
}
