﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class deleteWorker : Form
    {
        string FIO, ROLE;
        public deleteWorker(string fio, string role)
        {
            InitializeComponent();
            FIO = fio;
            ROLE = role;
            using (SqlConnection sqlConnection = new SqlConnection(connetion))
            {
                sqlDataAdapter = new SqlDataAdapter("select Фамилия,Имя, Отчество from Сотрудники where Код_должности = 1 and Блокировка = 0", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i =0; i < dataSet.Tables[0].Rows.Count;i++)
                {
                    comboBox1.Items.Add(dataSet.Tables[0].Rows[i][0].ToString() + " " + dataSet.Tables[0].Rows[i][1].ToString() + " " + dataSet.Tables[0].Rows[i][2].ToString());
                }
            }
        }
        Profile profile;
        string connetion = "Data Source = localhost; Initial Catalog = Бассейн_Иванов1; Integrated Security = true";
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        private void deleteWorker_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(FIO, ROLE);
            this.Hide();
            profile.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connetion))
            {
                if (comboBox1.SelectedIndex > -1)
                {
                    sqlDataAdapter = new SqlDataAdapter($"update Сотрудники set Блокировка = 1 where Фамилия + ' ' + Имя + ' ' +  Отчество = '{comboBox1.Text}'", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    MessageBox.Show("Вы удалили пользователя!");
                    profile = new Profile(FIO, ROLE);
                    this.Hide();
                    profile.Show();
                }
                else
                {
                    MessageBox.Show("Вы не выбрали пользователя для удаления!");
                }

            }
        }
    }
}
