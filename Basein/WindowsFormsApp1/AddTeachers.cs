﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class AddTeachers : Form
    {
        string Fio, ROLE;
        public AddTeachers(string FIO, string Role)
        {
            InitializeComponent();
            Fio = FIO;
            ROLE = Role;
        }
        string connect = "Data Source = localhost;Initial Catalog = Бассейн_Иванов1;Integrated Security = true";
      
        SqlCommand sqlCommand;
        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connect))
            {
                string commText = "insert into Сотрудники(Фамилия, Имя, Отчество, Номер_телефона, Код_должности, Логин, Пароль)values (@surname, @name, @lastname,@telephone,@work,@login,@password)";
                sqlCommand = new SqlCommand(commText,connection);
                sqlCommand.Parameters.AddWithValue("@surname",textBox1.Text);
                sqlCommand.Parameters.AddWithValue("@name", textBox2.Text);
                sqlCommand.Parameters.AddWithValue("@lastname", textBox3.Text);
                sqlCommand.Parameters.AddWithValue("@telephone", maskedTextBox1.Text);
                sqlCommand.Parameters.AddWithValue("@work", "1");
                sqlCommand.Parameters.AddWithValue("@login", textBox4.Text);
                sqlCommand.Parameters.AddWithValue("@password", textBox6.Text);
                connection.Open();
                try
                {
                    sqlCommand.ExecuteNonQuery();
                    MessageBox.Show("Вы удачно добавили сотрудника!", "Операция выполнена!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    profile = new Profile(Fio, ROLE);
                    this.Hide();
                    profile.Enabled = true;
                    profile.Show();
                }
                catch
                {
                    MessageBox.Show("Добавить не удалось!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }
        Profile profile;
        private void AddTeachers_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(Fio,ROLE);
            this.Hide();
            profile.Show();
        }
    }
}
