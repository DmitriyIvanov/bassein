﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace WindowsFormsApp1
{
    public partial class addTickets : Form
    {
        string Fio, ROLE;
        public addTickets(string FIO, string Role)
        {
            InitializeComponent();
            Fio = FIO;
            ROLE = Role;
        }
        string connect = "Data Source = localhost;Initial Catalog = Бассейн_Иванов1;Integrated Security = true";

        SqlCommand sqlCommand;
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        private void button1_Click(object sender, EventArgs e)
        {
            bool CheckName = false;
            using (SqlConnection connection = new SqlConnection(connect))
            {
                sqlDataAdapter = new SqlDataAdapter("select* from Группы", connection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                string commText = "insert into Абонементы(Количество_занятий, Стоимость)values(@count,@Cost)";
                sqlCommand = new SqlCommand(commText, connection);
                sqlCommand.Parameters.AddWithValue("@count", textBox1.Text);
                sqlCommand.Parameters.AddWithValue("@Cost", textBox2.Text);
                string name = textBox1.Text;
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (name == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        MessageBox.Show("Такой абонемент уже есть!");
                        textBox1.Text = "";
                        textBox2.Text = "";
                        break;
                    }
                    else
                    {
                        CheckName = true;
                    }
                }
                connection.Open();
                if (CheckName == true)
                {
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Вы удачно добавили абонемент!", "Операция выполнена!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        profile = new Profile(Fio, ROLE);
                        this.Hide();
                        profile.Enabled = true;
                        profile.Show();
                    }
                    catch
                    {
                        MessageBox.Show("Добавить не удалось!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
        }
        Profile profile;

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void addTickets_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = new Profile(Fio, ROLE);
            this.Hide();
            profile.Show();
        }

      
    }
}
